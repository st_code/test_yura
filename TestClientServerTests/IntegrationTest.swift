//
//  IntegrationTest.swift
//  TestClientServer
//
//  Created by werzul on 21/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import XCTest
@testable import TestClientServer

extension String: Error {}

class IntegrationTest: XCTestCase {
    
    var server: Server!
    var client: Client!
    
    override func setUp() {
        super.setUp()
        
        server = Server.shared
        let responderOK = RequestResponder { (request) -> Response in
            return Response(status: .ok, content: nil)
        }
        let responderCrash = RequestResponder { (request) -> Response in
            throw "Error thrown"
        }
        let responderBigData = RequestResponder { (request) -> Response in
            return Response(status: .ok, content: Data.init(count: 10000000))
        }
        server.router.register(route: Route.init(path: "testOK"), responder: responderOK)
        server.router.register(route: Route.init(path: "testCrash"), responder: responderCrash)
        server.router.register(route: Route.init(path: "testBigData"), responder: responderBigData)
        
        
        let requestRetranslator = RequestResponder { (request) -> Response in
            return Response(status: .ok, content: request.content)
        }
        
        server.router.register(route: Route.init(path: "requestMatching"), responder: requestRetranslator)
        
        client = Client()
    }
    
    func testOkResponse() {
        let request = Request(url: URL(string: "test.com/testOK")!, method: .GET)
        
        let responseExpect = expectation(description: "expect OK response")
        
        var resp: Response? = nil
        let _ = client.sendRequest(request: request) { (response) in
            resp = response
            responseExpect.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (_) in
            XCTAssertEqual(resp?.status, .ok, "ok status")
        }
    }
    
    func testNotFound() {
        let request = Request(url: URL(string: "test.com/notFoundPath")!, method: .GET)
        
        let responseExpect = expectation(description: "expect NOT FOUND response")
        
        var resp: Response? = nil
        let _ = client.sendRequest(request: request) { (response) in
            resp = response
            responseExpect.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (_) in
            XCTAssertEqual(resp?.status, .notFound, "notFound status")
        }
    }
    
    func testInternalError() {
        let request = Request(url: URL(string: "test.com/testCrash")!, method: .GET)
        
        let responseExpect = expectation(description: "expect Internal Error response")
        
        var resp: Response? = nil
        let _ = client.sendRequest(request: request) { (response) in
            resp = response
            responseExpect.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (_) in
            XCTAssertEqual(resp?.status, .internalError, "Internal Error status")
        }
    }
    
    func testBigDataOperation() {
        let request = Request(url: URL(string: "test.com/testBigData")!, method: .GET)
        
        let responseExpect = expectation(description: "expect response")
        
        var resp: Response? = nil
        let _ = client.sendRequest(request: request) { (response) in
            resp = response
            responseExpect.fulfill()
        }
        
        waitForExpectations(timeout: 10) { (_) in
            XCTAssertEqual(resp?.status, .ok, "ok status")
            XCTAssertEqual(resp?.content?.data.count, 10000000, "big data")
        }
    }
    
    func testCancelOperation() {
        let request = Request(url: URL(string: "test.com/testBigData")!, method: .GET)
        
        let operation = client.sendRequest(request: request) { (response) in
            
        }
        
        operation.cancel()
        
        XCTAssertEqual(operation.session!.isCancelled(), true, "all streams are closed")
    }
    
}
