//
//  ConnectionTestCase.swift
//  TestClientServer
//
//  Created by werzul on 21/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import XCTest
@testable import TestClientServer

class ConnectionTestCase: XCTestCase {
    
    var sendExpect: XCTestExpectation? = nil
    
    func testDataSend() {
        let connection = NetworkConnection()
        
        connection.delegate = self
        
        let data = Data.init(count: 70000)
        connection.send(data: data)
        
        sendExpect = expectation(description: "data received")
        waitForExpectations(timeout: 20) { (_) in
            
            XCTAssertEqual(connection.dataToReceive, data, "data should be equal")
        }
    }
    
    func testSmallDataSend() {
        let connection = NetworkConnection()
        
        connection.delegate = self
        
        let data = Data.init(count: 50)
        connection.send(data: data)
        
        sendExpect = expectation(description: "data received")
        waitForExpectations(timeout: 20) { (_) in
            
            XCTAssertEqual(connection.dataToReceive, data, "data should be equal")
        }
    }
    
}

extension ConnectionTestCase: ConnectionDelegate {
    func dataDidReceived(data: Data, connection: NetworkConnection) {
        sendExpect?.fulfill()
    }
}
