//
//  RequestTestCase.swift
//  TestClientServer
//
//  Created by werzul on 20/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import XCTest
@testable import TestClientServer

class RequestTestCase: XCTestCase {
    
    func testRequestToData() {
        let url = URL(string: "test.com")!
        let method = Request.Method.POST
        let headers = ["testHeader": "value"]
        let content: DataConvertible? = Data()
        
        let request = Request(url: url, method: method, headers: headers, content: content)
        
        let requestData = request.data
        
        let requestFromData = Request(data: requestData)!
        XCTAssertEqual(requestFromData.url, url, "equal url")
        XCTAssertEqual(requestFromData.method, method, "equal method")
        XCTAssertEqual(requestFromData.headers, headers, "equal headers")
        XCTAssertNotNil(requestFromData.content, "equal content")
    }
    
}
