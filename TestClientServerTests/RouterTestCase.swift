//
//  RouterTestCase.swift
//  TestClientServer
//
//  Created by werzul on 20/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import XCTest
@testable import TestClientServer

class RouterTestCase: XCTestCase {
    
    var router: Router!
    
    override func setUp() {
        super.setUp()
        
        router = Router()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testRouterAdd() {
        
        let routeGet = Route(path: "path", method: .GET)
        let routePost = Route(path: "path", method: .POST)
        
        router.register(route: routeGet) { (_) -> Response in
            return Response()
        }
        router.register(route: routePost) { (_) -> Response in
            return Response()
        }
        
        XCTAssertEqual(router.routes.keys.count, 2)
    }
    
    func testRouter() {
        
        router.register(route: Route(path: "path", method: .GET)) { (_) -> Response in
            return Response(status: .ok)
        }
        
        let requestGet = Request(url: URL(string: "test.com/path")!, method: .GET)
        
        let requestResponder = router.responderForRequest(request: requestGet)
        XCTAssertNotNil(requestResponder, "should NOT be nil")
        
        let response = try! requestResponder.responseTo(request: requestGet)
        XCTAssertEqual(response.status, .ok, "should receive OK response")
    }
    
    func testRouterNotFound() {
        
        let requestPost = Request(url: URL(string: "test.com/path")!, method: .POST)
        
        
        let notFountResponder = router.responderForRequest(request: requestPost)
        
        XCTAssertNotNil(notFountResponder, "should NOT be nil: not found")
        
        let response = try? notFountResponder.responseTo(request: requestPost)
        let status = response!.status
        XCTAssertEqual(status, .notFound, "should be available 404 not found")
    }
    
}
