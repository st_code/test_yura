//
//  Router.swift
//  TestClientServer
//
//  Created by werzul on 19/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit

/// route path + method
class Route: Equatable, Hashable {
    var method: Request.Method = .GET
    var path: String = ""
    
    public init(path: String, method: Request.Method = .GET) {
        self.path = path
        self.method = method
    }
    
    var hashValue: Int {
        return path.hashValue + method.hashValue
    }
}

func ==(lhs: Route, rhs: Route) -> Bool {
    return lhs.method == rhs.method &&
        lhs.path == rhs.path
}

/// ruler for route match responder
class Router {
    
    private(set) var routes: [Route: Responder] = [:]
    
    public func register(route: Route, responder: Responder) {
        routes[route] = responder
    }
    
    public func responderForRequest(request: Request) -> Responder {
        if let responder = routes[request.route] {
            return responder
        } else {
            return NotFoundResponder()
        }
    }
}

extension Router {
    public func register(route: Route, responseBlock: @escaping RequestResponder.ResponseBlock) {
        self.register(route: route, responder: RequestResponder(responseBlock: responseBlock))
    }
}
