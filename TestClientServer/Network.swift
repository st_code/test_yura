//
//  Network.swift
//  TestClientServer
//
//  Created by werzul on 21/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit

/// Abstract network class
class Network {
    
    // Commutation client and server
    func commute(client: Client, server: Server) -> NetworkSession {
        
        let session = NetworkSession()
        session.requestDelegate = server
        session.responseDelegate = client
        
        return session
    }

}
