//
//  Request.swift
//  TestClientServer
//
//  Created by werzul on 19/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol DataConvertible {
    
    init?(data: Data)
    
    var data: Data { get }
}

extension Data: DataConvertible {
    init?(data: Data) {
        self.init()
        self.append(data)
    }
    
    internal var data: Data {
        return self
    }
}

final class Request {
    
    /// methods
    enum Method: String {
        case GET = "GET"
        case POST = "POST"
    }
    public var url: URL
    public var method: Method = .GET
    public var headers: [String: String] = [:]
    public var content: DataConvertible? = nil
    
    init(url: URL, method: Method = .GET, headers: [String: String] = [:], content: DataConvertible? = nil) {
        self.url = url
        self.method = method
        self.headers = headers
        self.content = content
    }
}

/// For request to send via stream
extension Request: DataConvertible {
    convenience init?(data: Data) {
        let jsonData = JSON(data: data)
        guard let url = URL(string: jsonData["_url"].stringValue) else { return nil }
        
        self.init(url: url)
        
        let method = Method(rawValue: jsonData["_method"].stringValue) ?? .GET
        self.method = method
        
        self.headers = jsonData["_headers"].stringDictionaryValue
        self.content = Data(base64Encoded: jsonData["_content"].stringValue)
    }
    
    var data: Data {
        let contentString = self.content?.data.base64EncodedString() ?? ""
        let jsonData: JSON = ["_url": url.absoluteString,
                              "_method": method.rawValue,
                              "_headers": headers,
                              "_content": contentString]
        do {
            return try jsonData.rawData()
        } catch {
            return Data()
        }
    }
}

extension Request {
    var route: Route {
        
        return Route(path: url.lastPathComponent, method: method)
    }
}

extension JSON {
    
    //Optional [String : String]
    public var stringDictionaryValue: [String : String] {
        let dictionary = self.dictionaryValue
        var stringDictionary = [String : String]()
        
        for key in dictionary.keys {
            if let value = dictionary[key] {
                stringDictionary[key] = value.stringValue
            }
        }
        
        return stringDictionary
    }
}
