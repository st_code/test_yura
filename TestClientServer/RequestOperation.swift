//
//  RequestOperation.swift
//  TestClientServer
//
//  Created by werzul on 20/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit

/// Operation that contains request and session
class RequestOperation: Equatable {
    
    internal var guid: String = UUID().uuidString
    
    typealias ResponseHandler = (Response) -> ()
    
    private var request: Request
    private var responseHandler: ResponseHandler
    
    /// network session with server
    var session: NetworkSession? = nil
    
    init(request: Request, handler: @escaping ResponseHandler) {
        self.request = request
        self.responseHandler = handler
    }
    
    public func handle(response: Response) {
        responseHandler(response)
    }
    
    public func start(session: NetworkSession) {
        self.session = session
        
        session.sendRequest(data: request.data)
    }
    
    public func cancel() {
        session?.close()
    }
}

internal func ==(lhs: RequestOperation, rhs: RequestOperation) -> Bool {
    return lhs.guid == rhs.guid
}

