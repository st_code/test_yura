//
//  Server.swift
//  TestClientServer
//
//  Created by werzul on 19/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit

/// ruler for route match responder
class Server {
    
    static private var sharedInstanse: Server? = nil
    
    static var shared: Server {
        if let sharedInstanse = self.sharedInstanse {
            return sharedInstanse
        } else {
            let instanse = Server()
            self.sharedInstanse = instanse
            return instanse
        }
    }
    
    var router = Router()
    
    internal func handle(request: Request) -> Response {
        let responder = router.responderForRequest(request: request)
        
        let response: Response
        
        do {
            response = try responder.responseTo(request: request)
        } catch {
            response = Response(status: .internalError)
        }
        
        return response
    }
}

extension Server: SessionDelegate {
    
    func dataDidReceived(data: Data, session: NetworkSession) {
        if let request = Request(data: data) {
            didReceiveRequest(request: request, session: session)
        }
    }
}

extension Server {
    
    func didReceiveRequest(request: Request, session: NetworkSession) {
        
        let response = self.handle(request: request)
        
        session.sendResponse(data: response.data)
    }
}

extension Server {
    public func run() {
        // setup ports and other stuff to run server
        // let r = RunLoop.init()
    }
}
