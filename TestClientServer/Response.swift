//
//  Response.swift
//  TestClientServer
//
//  Created by werzul on 19/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Status: Int {
    case ok = 200
    case notFound = 404
    case internalError = 500
    // http status codes
}

final class Response {
    var status: Status = .ok
    var content: DataConvertible? = nil
    
    init(status: Status = .ok, content: DataConvertible? = nil) {
        self.status = status
        self.content = content
    }
}

extension Response: DataConvertible {
    internal convenience init?(data: Data) {
        let json = JSON(data: data)
        let status = Status(rawValue: json["_status"].intValue) ?? .ok
        let content = Data(base64Encoded: json["_content"].stringValue)
        
        self.init(status: status, content: content)
    }

    var data: Data {
        let contentString = self.content?.data.base64EncodedString()
        let jsonData: JSON = ["_status": status.rawValue,
                              "_content": contentString ?? ""]
        if let data = try? jsonData.rawData() {
            return data
        } else {
            return Data()
        }
    }
}
