//
//  ReqestManager.swift
//  TestClientServer
//
//  Created by werzul on 20/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit

/// class that represents client side of application
class Client {
    
    internal var operations: [RequestOperation] = []
    
    func sendRequest(request: Request, handler: @escaping RequestOperation.ResponseHandler) -> RequestOperation {
        let requestOperation = RequestOperation(request: request, handler: handler)
        
        self.startOperation(operation: requestOperation)
        return requestOperation
    }
    
    func startOperation(operation: RequestOperation) {
        let session = Network().commute(client: self, server: Server.shared)
        operation.start(session: session)
        
        self.operations.append(operation)
    }
    
    func cancelOperation(operation: RequestOperation) {
        operation.cancel()
    }
    
    func operationForSession(session: NetworkSession) -> RequestOperation? {
        
        let operation = self.operations.first { (operation) -> Bool in
            return session == operation.session!
        }
        return operation
    }
}

extension Client: SessionDelegate {
    
    func dataDidReceived(data: Data, session: NetworkSession) {
        if let response = Response(data: data) {
            didReceiveResponse(response: response, session: session)
        }
    }
}

extension Client {
    
    func didReceiveResponse(response: Response, session: NetworkSession) {
        if let operation = self.operationForSession(session: session) {
            operation.handle(response: response)
        }
    }
}
