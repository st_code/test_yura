//
//  Responder.swift
//  TestClientServer
//
//  Created by werzul on 20/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import Foundation

/// interface that generate response from request
protocol Responder {
    func responseTo(request: Request) throws -> Response
}

final class NotFoundResponder: Responder {
    func responseTo(request: Request) throws -> Response {
        return Response(status: .notFound)
    }
}

class RequestResponder: Responder {
    
    typealias ResponseBlock = ((Request) throws -> Response)
    
    private var responseBlock: ResponseBlock
    
    init(responseBlock: @escaping ResponseBlock) {
        self.responseBlock = responseBlock
    }
    
    func responseTo(request: Request) throws -> Response {
        return try responseBlock(request)
    }
}
