//
//  Connection.swift
//  TestClientServer
//
//  Created by werzul on 19/03/17.
//  Copyright © 2017 werzul. All rights reserved.
//

import UIKit


protocol SessionDelegate: class {
    func dataDidReceived(data: Data, session: NetworkSession)
}

class NetworkSession: ConnectionDelegate {
    
    internal var guid: String = UUID().uuidString
    
    public var requestDelegate: SessionDelegate? = nil
    public var responseDelegate: SessionDelegate? = nil
    
    
    internal var requestConnection: NetworkConnection
    internal var responseConnection: NetworkConnection
    
    init() {
        self.requestConnection = NetworkConnection()
        self.responseConnection = NetworkConnection()
        
        self.requestConnection.delegate = self
        self.responseConnection.delegate = self
    }
    
    func sendRequest(data: Data) {
        self.requestConnection.send(data: data)
    }
    
    func sendResponse(data: Data) {
        self.responseConnection.send(data: data)
    }
    
    func close() {
        self.requestConnection.close()
        self.responseConnection.close()
    }
    
    func dataDidReceived(data: Data, connection: NetworkConnection) {
        if connection == requestConnection {
            requestDelegate?.dataDidReceived(data: data, session: self)
        }
        
        if connection == responseConnection {
            responseDelegate?.dataDidReceived(data: data, session: self)
        }
        
    }
}

extension NetworkSession {
    public func isCancelled() -> Bool {
        let status1 = self.requestConnection.inputStream!.streamStatus
        let status2 = self.requestConnection.outputStream!.streamStatus
        let status3 = self.responseConnection.inputStream!.streamStatus
        let status4 = self.responseConnection.outputStream!.streamStatus
        
        return  (status1 == .closed || status1 == .notOpen) &&
            (status2 == .closed || status2 == .notOpen) &&
            (status3 == .closed || status3 == .notOpen) &&
            (status4 == .closed || status4 == .notOpen)
    }
}

protocol ConnectionDelegate: class {
    func dataDidReceived(data: Data, connection: NetworkConnection)
}


internal func ==(lhs: NetworkSession, rhs: NetworkSession) -> Bool {
    return lhs.guid == rhs.guid
}


/// Connection between client and server
class NetworkConnection: NSObject {
    
    weak var delegate: ConnectionDelegate? = nil
    
    let buferSize = 65536
    
    /// fake input bufer
    internal var dataToSend = Data()
    /// fake output bufer
    internal var dataToReceive = Data()
    
    /// streams to create pipe between sender and receiver
    internal var inputStream: InputStream? = nil
    internal var outputStream: OutputStream? = nil
    
    override init() {
        super.init()
        
        Stream.getBoundStreams(withBufferSize: buferSize, inputStream: &inputStream, outputStream: &outputStream)
        
        outputStream?.delegate = self
        inputStream?.delegate = self
        
        inputStream?.schedule(in: RunLoop.current, forMode: .defaultRunLoopMode)
        outputStream?.schedule(in: RunLoop.current, forMode: .defaultRunLoopMode)
    }
    
    public func send(data: Data) {
        self.dataToSend = data
        
        inputStream?.open()
        outputStream?.open()
    }
    
    public func close() {
        outputStream?.delegate = nil
        inputStream?.delegate = nil
        
        inputStream?.close()
        outputStream?.close()
    }
}

extension NetworkConnection: StreamDelegate {
    
    func stream(_ aStream: Stream, handle eventCode: Stream.Event) {
        
        if aStream == self.outputStream {
            
            if eventCode == .hasSpaceAvailable {
                
                if self.dataToSend.count > 0 {
                    self.writeData()
                } else {
                    aStream.close()
                }
            }
        }
        
        if aStream == self.inputStream {
            if eventCode == .hasBytesAvailable {
                self.readData()
            }
            if eventCode == .endEncountered {
                self.delegate?.dataDidReceived(data: self.dataToReceive, connection: self)
                aStream.close()
                
            }
        }
    }
    
    func writeData() {
        let dataLength = min(self.dataToSend.count, buferSize)
        var range: Range<Data.Index> = 0..<dataLength
        let data = self.dataToSend.subdata(in: range)
        let _ = data.withUnsafeBytes { return outputStream?.write($0, maxLength: data.count) }
        
        range = dataLength..<self.dataToSend.count
        self.dataToSend = self.dataToSend.subdata(in: range)
    }
    
    func readData() {
        var inputData = Data(count: buferSize)
        let count = inputData.withUnsafeMutableBytes { return inputStream?.read($0, maxLength: buferSize) } ?? -1
        
        if count == -1 {
            self.close()
        }
        
        let range: Range<Data.Index> = 0..<count
        let readData = inputData.subdata(in: range)
        self.dataToReceive.append(readData)
    }
}
